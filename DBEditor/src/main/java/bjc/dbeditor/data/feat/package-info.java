/**
 * Contains the data types to do with feats
 * 
 * @author ben
 *
 */
package bjc.dbeditor.data.feat;